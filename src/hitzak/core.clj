(ns hitzak.core)

(def ^:private sub20-words ["zero" "one" "two" "three" "four" "five" "six" "seven"
                            "eight" "nine" "ten" "evelen" "twelve" "thirteen" "fourteen"
                            "fifteen" "sixteen" "seventeen" "eigtheen" "nineteen"])

(def ^:private tens-words ["twenty" "thirty" "forty" "fifty" "sixty" "seventy" "eigthy" "ninety"])

(defn- valid-entry?
  [x]
  (and (integer? x) (<= x 999999999)))

(defn- num->tens
  "Converts a number between 20 and 99 to British English words"
  [n]
  (let [tens-digit (quot n 10)
        units-digit (rem n 10)
        whole-ten? (zero? units-digit)]
    (if (< n 20)
      (nth sub20-words n)
      (str (nth tens-words (- tens-digit 2))
           (when (not whole-ten?) (str " " (nth sub20-words units-digit)))))))

(defn- whole-hundred->word
  "Converts an integer number that is a multiple of 100 into British English words"
  [x]
  (str (nth sub20-words x) " hundred"))

(defn- hundred->word
  [x]
  (let [hundred-part (quot x 100)
        tens-part (rem x 100)
        whole-hundred? (zero? tens-part)]
    (if (zero? hundred-part)
      (num->tens x)
      (str (whole-hundred->word hundred-part)
           (when (not whole-hundred?) (str " and " (num->tens tens-part)))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn magnitude
  [x]
  (loop [magnitude 1]
    (if (> (Math/pow 10 magnitude) x)
      (Math/floor (/ magnitude 3))
      (recur (inc magnitude)))))

(def big-numbers ["thousand" "million" "billion" "trillion"])

(defn write111
  "A smaller, recursive alternative (WIP)"
  [x]
 (let [magn (magnitude x)
       triad (quot x (Math/pow 10 (* 3 (dec magn))))
       remaining (rem x (Math/pow 10 (* 3 (dec magn))))]
   (cond
     (< x 100) (str " and " (hundred->word x))
     (<= 100 x 999) (hundred->word x)
     :else (str (hundred->word triad) " " (nth big-numbers (- magn 2)) " " (write111 remaining)))))

;;(write111 123456789)
;"one hundred and twenty three million four hundred and fifty six thousand seven hundred and eigthy nine"

;;(write111 123000)
;;"one hundred and twenty three thousand  and zero"

;;(write111 123000000)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn- thousand->word
  [x]
  (let [thousand-part (quot x 1000)
        hundred-part (rem x 1000)
        whole-thousand? (zero? hundred-part)
        thousand-connector (if (< 100 hundred-part) " thousand " " thousand and ")]
    (if whole-thousand?
      (str (nth sub20-words thousand-part) " thousand")
      (if (not (zero? thousand-part))
        (str (hundred->word thousand-part) thousand-connector (hundred->word (rem x 1000)))
        (str "and " (hundred->word (rem x 1000)))))))

(defn- million->word
  [x]
  (let [million-part (quot x 1000000)
        thousand-part (quot (rem x 1000000) 1000)
        hundred-part (rem x 1000)
        whole-million? (and (zero? thousand-part) (zero? hundred-part))]
    (if whole-million?
      (str (nth sub20-words million-part) " million")
      (str (hundred->word million-part) " million " (thousand->word (rem x 1000000))))))

(defn writeout
  "Converts ain integer number between 0 and 999999999 to British English words"
  [x]
  (if (not (valid-entry? x))
    (throw (Exception. "Invalid entry. Only integers between 0 and 999999999 are accepted."))
    (cond
      (< x 1000) (hundred->word x)
      (< x 999999) (thousand->word x)
      :else (million->word x))))

