(defproject org.clojars.iggar/hitzak "0.1.2"
  :description "A library to convert integer numbers in the range 0-999999999 to British English words"
  :license {:name "Apache 2.0 License"
            :url "https://www.apache.org/licenses/LICENSE-2.0"}
  :dependencies [[org.clojure/clojure "1.8.0"]])
