# hitzak

A Clojure library to convert a number to words.
`hitzak` means "words" in Basque.


## Installation

=hitzak is available as a Maven artifact from [[http://clojars.org/hitzak][Clojars]].

With Leiningen/Boot:

```
[hitzak 0.1.2]
```

## Usage

The main functionality is provided by the core namespace.

First, require it in the REPL:

(require '[hitzak.core :as hitzak])

Or in your application:

```
(ns my-app.core
  (:require [hitzak.core :as hitzak]))
```

Then

```
(hitzak/writeout 1234567)
```


## License

Copyright © 2018 Igor Garcia

Distributed under the Apache 2.0 Licence.



