(ns hitzak.core-test
  (:require [clojure.test :refer :all]
            [hitzak.core :refer :all]))

(deftest test-invalid-letter
  (testing "a"
    (is (thrown? Exception (writeout "a")))))

(deftest test-invalid-mixed-digits-letters
  (testing "123abc"
    (is (thrown? Exception (writeout "123bc")))))

(deftest test-invalid-special-chars
  (testing "£$%&*"
    (is (thrown? Exception (writeout "£$%&*")))))

(deftest test-invalid-nil
  (testing "nil"
    (is (thrown? Exception (writeout nil)))))

(deftest test-0
  (testing "0"
    (is (= "zero" (writeout 0)))))

(deftest test-1
  (testing "1"
    (is (= "one" (writeout 1)))))

(deftest test-19
  (testing "19"
    (is (= "nineteen" (writeout 19)))))

(deftest test-21
  (testing "21"
    (is (= "twenty one" (writeout 21)))))

(deftest test-99
  (testing "99"
    (is (= "ninety nine" (writeout 99)))))

(deftest test-100
  (testing "100"
    (is (= "one hundred" (writeout 100)))))

(deftest test-105
  (testing "105"
    (is (= "one hundred and five" (writeout 105)))))

(deftest test-123
  (testing "123"
    (is (= "one hundred and twenty three" (writeout 123)))))

(deftest test-919
  (testing "919"
    (is (= "nine hundred and nineteen" (writeout 919)))))

(deftest test-999
  (testing "999"
    (is (= "nine hundred and ninety nine" (writeout 999)))))

(deftest test-1005
  (testing "one thousand and five"
    (is (= "one thousand and five" (writeout 1005)))))

(deftest test-1042
  (testing "one thousand and forty two"
    (is (= "one thousand and forty two" (writeout 1042)))))

(deftest test-1005
  (testing "one thousand one hundred and five"
    (is (= "one thousand one hundred and five" (writeout 1105)))))

(deftest test-2009
  (testing "two thousand and nine"
    (is (= "two thousand and nine" (writeout 2009)))))

(deftest test-56945781
  (testing "56945781"
    (is (= "fifty six million nine hundred and forty five thousand seven hundred and eigthy one"
           (writeout 56945781)))))

(deftest test-999999999
  (testing "999999999"
    (is (=(str "nine hundred and ninety nine million nine hundred "
               "and ninety nine thousand nine hundred and ninety nine")
               (writeout 999999999)))))

(deftest test-10000098
  (testing "10000098"
    (is (= "ten million and ninety eight"
           (writeout 10000098)))))


;; Should we validate / sanitise numbers in other bases? If we don't validate it, 0123456 means 42798
;(deftest test-octal-input
;  (testing "0123456"
;    (is (= (wholething 0123456)
;        "one hundred and twenty three thousand four hundred and fifty six"))))

